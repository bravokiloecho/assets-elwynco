const build = require('./build/index');
const utils = require('./build/_utils');
const env = utils.checkEnvFlag( process );
build.buildAll( env );