const path = require('path');
const fs = require('fs');

// UTILS
const setBuildConfig = function () {
	const _ = require('lodash');
	const configData = path.resolve(__dirname,'../', 'devAssets/buildConfig.yaml');

	return new Promise(function ( resolve, reject ) {

		readYaml( configData ).then(function ( data ) {
			
			_.forIn( data, function ( value, key ) {
				// console.log('key: '+key);
				// console.log('value: '+value);
				process.env[ key ] = value;
			});

			process.env.buildConfigSet = true;

			resolve();

		});
	});
};

const readYaml = function ( yamlFile ) {
	
	return new Promise(function ( resolve, reject ) {
		const yaml = require('node-yaml');
		yaml.read( yamlFile, {
			encoding: 'utf8',
			schema: yaml.schema.defaultSafe
		}, function ( err, data ) {
			
			if ( err ) {
				console.log('ERROR GETTING DATA');
				console.log(err);
				reject( err );
				return;
			}

			resolve( data );
		});
	});
};

const saveYaml = function ( file, data ) {

	return new Promise(function ( resolve, reject ) {
		const yaml = require('node-yaml');
		yaml.write(file, data, {}, function(err) {
			if (err) {
				console.error(err);
				reject();
			}
			// console.log('Saved as YAML');
			resolve('Saved as YAML');
		});
	});
};

const ensureDirectoryExistence = function ( filePath ) {
	const dirname = path.dirname(filePath);
	if (fs.existsSync(dirname)) {
		return true;
	}
	ensureDirectoryExistence(dirname);
	fs.mkdirSync(dirname);
};

const saveFile = function ( filePath, string ) {
	return new Promise(function ( resolve, reject ) {
		ensureDirectoryExistence( filePath );
		fs.writeFile( filePath, string, function(err) {
			console.log(`${ filePath } saved!`);
			resolve();
		});
	});
};

const copyDirectory = function ( source, destination ) {
	// https://github.com/jprichardson/node-fs-extra
	const fs = require('fs-extra');

	const options = {
		preserveTimestamps: true,
		overwrite: true
	};

	console.log('Start copying asset: ' + source + ' to ' + destination);

	return new Promise(function ( resolve, reject ) {
		fs.copy( source, destination, err => {
			if (err) {
				reject( err );
				return console.error(err);
			}
			console.log('success!');
			resolve('Copied directory: ' + source + ' to ' + destination);
			console.log('Copied directory: ' + source + ' to ' + destination);
		});
	});
};

const checkIfFileExists = function ( path ) {
	// console.log('*******************FILE EXISTS?');
	// console.log(path);
	// console.log(fs.existsSync( path ));
	return fs.existsSync( path );
};

const readFile = function ( path ) {
	return new Promise(function ( resolve, reject ) {
		fs.readFile( path, 'utf8', function (err,data) {
			if (err) {
				console.log(err);
				reject();
			}
			// console.log('*******************READ FILE');
			// console.log(data);
			resolve(data);
		});
	});
};

const checkEnvFlag = function ( proc ) {
	const processArgs = process.argv;
	const flag = processArgs[processArgs.length - 1];
	const defaultEnv = 'staging';
	
	if ( flag === '-d' ) {
		return 'dev';
	}

	if (flag === '-p'){
		return 'production';
	}

	return defaultEnv;
};

const checkRunFlag = function ( proc ) {
	const processArgs = process.argv;
	const flag = processArgs[processArgs.length - 1];
	if ( flag === '-r' || flag === '-b' ) {
		return true;
	} else {
		return false;
	}
};


module.exports = {
	setBuildConfig: setBuildConfig,
	ensureDirectoryExistence: ensureDirectoryExistence,
	saveFile: saveFile,
	copyDirectory: copyDirectory,
	checkIfFileExists: checkIfFileExists,
	readFile: readFile,
	readYaml: readYaml,
	saveYaml: saveYaml,
	checkEnvFlag: checkEnvFlag,
	checkRunFlag: checkRunFlag,
};