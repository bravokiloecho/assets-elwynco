const path = require('path');
const fs = require('fs');
const utils = require('./_utils');

const publicAssetsDir = './devAssets/publicAssets/';

// COPY GFX Across
const copyAssets = function () {
	return new Promise(function ( resolve, reject ) {

		const inputDir = publicAssetsDir;

		fs.readdir( inputDir, function( err, files ) {
		
			const allPromises = [];

			files.forEach(function ( file ) {

				const filePath = path.join( inputDir, file );

				console.log('file: '+file);
				console.log('filePath: '+filePath);
				if ( file === '.DS_Store' ) {
					return;
				}

				// Copy directories
				if ( fs.lstatSync( filePath ).isDirectory() ) {
					const dirName = path.parse( file ).name;
					const destination = `${ process.env.publicDir }${ dirName }/`;
					const copyDir = utils.copyDirectory( filePath, destination );
					// console.log(copyDir);
					allPromises.push( copyDir );
				}
				// Copy files
				else {
					const destination = `${ process.env.publicDir }${ file }/`;
					const copyFile = utils.copyDirectory( filePath, destination );
					// console.log(copyFile);
					allPromises.push( copyFile );	
				}

			});

			Promise.all( allPromises ).then( resolve );
		});

	});	 
};

// Check flags and run if necessary
if ( utils.checkRunFlag( process ) ) {
	utils.setBuildConfig().then( copyAssets );
}

module.exports = {
	copyAssets: copyAssets
};