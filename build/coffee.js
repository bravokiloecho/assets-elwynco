const utils = require('./_utils');
const coffeebar = require('coffeebar');

const compileCoffee = function ( env ) {

	const inputDir = './devAssets/coffee/';
	const outputDir = 'mouse';//env === 'dev' ? 'mouse' : 'min';
	
	return new Promise(function ( resolve, reject ) {

		var options = {
			output: `${ process.env.publicDir }js/${outputDir}/scripts.js`,
			minify: env === 'dev' ? false : true,
			sourceMap: env === 'dev' ? true : false,
			bare: true,
			watch: false,
			silent: true
		};

		coffeebar( inputDir, options );
		console.log('COMPILED COFFEE');
		resolve('COFFEE DONE');
	});
};

// Check flags and run if necessary
if ( utils.checkRunFlag( process ) ) {
	utils.setBuildConfig().then( compileCoffee );
}

module.exports = {
	compileCoffee: compileCoffee
};