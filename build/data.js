const path = require('path');
const fs = require('fs');
const yaml = require('node-yaml');
const jsonfile = require('jsonfile');

const utils = require('./_utils.js');

const saveData = function ( data ) {

	return new Promise(function ( resolve, reject ) {
		
		if ( data ) {
			saveJson( data ).then( resolve );
		} else {
			const contentDirectory = path.resolve(__dirname,'../', 'devAssets/contentData/');
			const siteContentFile = path.join( contentDirectory, 'siteContent.yaml');
			getData( siteContentFile, function ( data ) {
				saveJson( data ).then( resolve );
			});
		}
	});	
};

function getData ( file, next ) {
	yaml.read( file, {
		encoding: 'utf8',
		schema: yaml.schema.defaultSafe
	}, function ( err, data ) {
		next( data );
	});
}

function saveJson ( data ) {
	return new Promise(function ( resolve, reject ) {
		const json = JSON.stringify( data );
		const jsonFile = path.join( process.env.publicDir + 'data/', 'siteContent.json');
		var fileSaved = utils.saveFile( jsonFile, json );
		fileSaved.then( resolve );
	});
}

// Check flags and run if necessary
if ( utils.checkRunFlag( process ) ) {
	utils.setBuildConfig().then( saveData );
}

module.exports = {
	saveData: saveData
};