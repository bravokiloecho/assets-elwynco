const path = require('path');
const fs = require('fs');
const utils = require('./_utils.js');
const svg2png = require("svg2png");
const cheerio = require('cheerio');

const svgDir = './devAssets/svg/';
const gfxDir = './devAssets/publicAssets/gfx/';

// SVG minification
const minifySVGs = function () {
	const SVGO = require('svgo');
	
	// Options here: https://github.com/svg/svgo
	const svgo = new SVGO({
		plugins: [
			{mergePaths: false},
			{ removeDimensions: true}
		]
	});

	const inputDir = svgDir;
	const outputDir = gfxDir;

	return new Promise(function ( resolve, reject ) {

		// Loop through all sass files
		fs.readdir( inputDir, function( err, files ) {
			if ( err ) {
				console.error( "Could not list the directory.", err );
				process.exit( 1 );
			}

			var allFilePromises = [];

			files.forEach( function( file, index ) {
				var filePromise = minifySVG( file, svgo, inputDir, outputDir );
				allFilePromises.push( filePromise );				
			});

			Promise.all( allFilePromises ).then(function () {
				resolve();
			});

		});

	});
};

function minifySVG ( file, svgo, inputDir, outputDir ) {
	const filePath = path.join( inputDir, file );
	const fileName = file;

	return new Promise(function ( resolve, reject ) {

		const extension = path.extname( filePath );
		if ( extension !== '.svg') {
			resolve();
			return;
		}

		fs.readFile(filePath, 'utf8', function(err, data) {

			if (err) {
				throw err;
			}
			
			console.log('minify');
			
			svgo.optimize(data).then(function( result ) {
				const svgString = result.data;
				const fileSaved = utils.saveFile( outputDir + fileName, svgString );
				console.log("MINIFIED SVGs");
				
				fileSaved.then( resolve );
			});
		});
	});
}


// SVG minification
const convertSVGs = function () {
	
	const inputDir = svgDir;
	const outputDir = gfxDir;

	console.log('convertSVGs');
	

	return new Promise(function ( resolve, reject ) {

		// Loop through all sass files
		fs.readdir( inputDir, function( err, files ) {
			if ( err ) {
				console.error( "Could not list the directory.", err );
				process.exit( 1 );
			}

			var allFilePromises = [];

			files.forEach( function( file, index ) {
				var filePromise = convertSvgToPng( file, inputDir, outputDir );
				allFilePromises.push( filePromise );				
			});

			Promise.all( allFilePromises ).then(function () {
				resolve();
			});

		});

	});
};


function convertSvgToPng ( file, inputDir, outputDir ) {
	const filePath = path.join( inputDir, file );
	const fileName = file.replace('.svg','.png');
	const pnfs = require("pn/fs"); // https://www.npmjs.com/package/pn

	return new Promise(function ( resolve, reject ) {

		const extension = path.extname( filePath );
		if ( extension !== '.svg') {
			resolve();
			return;
		}

		pnfs.readFile( filePath )
			.then( addSvgDims )
			.then( svg2png )
			.then(buffer => fs.writeFile( outputDir + fileName , buffer, (error) =>
				{ if ( error ) { console.log("Error!");} })
			)
			.then( resolve )
			.catch(e => console.error(e));
	});

}

function addSvgDims ( svg ) {
	return new Promise(function ( resolve, reject ) {
		const $ = cheerio.load( svg );
		// const $svg = $('svg').first();
		// If height and width present, stop here
		if ( !$('svg').attr('width') || $('svg').attr('height') ) {
			const viewboxDims = $('svg').attr('viewBox').split(' ');
			$('svg').attr('width', viewboxDims[2] + 'px' );
			$('svg').attr('height', viewboxDims[3] + 'px' );
		}

		resolve( $.html() );
		    
	});
}

// COPY GFX Across
const copyGfx = function () {
	const source = gfxDir;
	const destination = process.env.publicDir + 'gfx/';

	return utils.copyDirectory( source, destination );
};

// Check flags and run if necessary
if ( utils.checkRunFlag( process ) ) {
	utils.setBuildConfig()
		.then( minifySVGs )
		.then( convertSVGs )
		.then( copyGfx );
}

module.exports = {
	minifySVGs: minifySVGs,
	convertSVGs: convertSVGs,
	copyGfx: copyGfx
};