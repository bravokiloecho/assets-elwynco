// IMPORT BUILD MODULES
const utils = require('./_utils.js');
const data = require('./data.js');
// DEFINE MODULES
const coffee = require('./coffee.js');
const assets = require('./assets.js');
const gfx = require('./gfx.js');
const js = require('./js.js');
const sass = require('./sass.js');
const pug = require('./pug.js');

// BUILD ALL
const buildAll = function ( env, next ) {

	return new Promise(function ( buildResolve, buildReject ) {

		// If build config already done
		if ( process.env.buildConfigSet ) {
			// console.log('BUILD NO READ CONFGIG');
			runBuild( env ).then(function () {
				console.log('DONE BUILDING');
				buildResolve();
				if ( next ) {
					next();
				}
			});
		} else {
			// console.log('BUILD READ CONFGIG');
			utils.setBuildConfig().then(function () {
				runBuild( env ).then(function () {
					console.log('DONE BUILDING');
					buildResolve();
					if ( next ) {
						next();
					}
				});
			});
		}
	});
};

function runBuild ( env ) {
	
	var allPromises = [];
	var counter = {
		total: 0,
		names: []
	};
	
	return new Promise(function ( resolve, reject ) {

		// Static specific build types
		if ( process.env.buildType === 'static' ) {
			
			// var dataPromise = data.saveData();
			// allPromises.push( dataPromise );
			// testPromise( dataPromise, 'dataPromise', counter );
			var htmlPromise = pug.buildHtml( env, true );
			allPromises.push( htmlPromise );
			// testPromise( htmlPromise, 'htmlPromise', counter );
		}		

		var coffeePromise = coffee.compileCoffee( env );
		allPromises.push( coffeePromise );
		// testPromise( coffeePromise, 'coffeePromise', counter );
		
		var pluginPromise = js.compilePlugins( env );
		allPromises.push( pluginPromise );
		// testPromise( pluginPromise, 'pluginPromise', counter );

		var browserifyPromise = js.runBrowserify( env );
		allPromises.push( browserifyPromise );
		// testPromise( browserifyPromise, 'browserifyPromise', counter );

		if ( env !== 'dev' ) {
			var jsPromise = new Promise(function ( resolve, reject ) {
				Promise.all([ coffeePromise, pluginPromise, browserifyPromise ]).then(values => { 
					var compilePromise = js.compileAllJS();
					compilePromise.then( resolve );
				});
			});
			allPromises.push( jsPromise );
			// testPromise( jsPromise, 'jsPromise', counter );
		}

		var sassPromise = sass.compileAllSass( env );
		allPromises.push( sassPromise );
		// testPromise( sassPromise, 'sassPromise', counter );

		var svgPromise = gfx.minifySVGs();
		allPromises.push( svgPromise );
		// testPromise( svgPromise, 'svgPromise', counter );

		// CONVERT PNGs
		// if ( env !== 'dev' ) {
		// 	var pngPromise = gfx.convertSVGs();
		// 	allPromises.push( pngPromise );
		// }

		var gfxPromise = new Promise(function ( resolve, reject ) {
			svgPromise.then(function () {
				var copied = gfx.copyGfx();
				copied.then( resolve );
			});
		});
		allPromises.push( gfxPromise );
		// testPromise( gfxPromise, 'gfxPromise', counter );

		var assetsPromise = new Promise(function ( resolve, reject ) {
			
			const promises = [ svgPromise, gfxPromise ];
			
			// WAIT FOR PNG creation
			// if ( env !== 'dev' ) {
			// 	promises.push( pngPromise );
			// }

			Promise.all( promises ).then(values => { 
				console.log('COPY ASSETS');
				assets.copyAssets().then( resolve );
			});
		});
		allPromises.push( assetsPromise );
		// testPromise( assetsPromise, 'assetsPromise', counter );
		
		Promise.all( allPromises )
			.then( resolve )
			.catch(function (err ) {
				console.log('BUILD CATCH');
				console.log(err);
			});

	});
}

function testPromise ( promise, name, counter ) {
	promise.then(function () {
		counter.total++;
		console.log(counter.total + ': gfxPromise');
		counter.names.push( name );
		console.log(counter.names);
		console.log(counter.names.length);
		
	}).catch(function ( err ) {
		console.log('TEST PROMISE ERROR');
		console.log(err);
		console.log(name);
	});
}

module.exports = {
	buildAll: buildAll,
	saveData: data.saveData, 
	buildHtml: pug.buildHtml,
	compileCoffee: coffee.compileCoffee,
	compilePlugins: js.compilePlugins,
	runBrowserify: js.runBrowserify,
	compileAllJS: js.compileAllJS,
	compileAllSass: sass.compileAllSass,
	minifySVGs: gfx.minifySVGs,
	copyGfx: gfx.copyGfx,
	copyAssets: assets.copyAssets
};