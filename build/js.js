const path = require('path');
const fs = require('fs');
const utils = require('./_utils.js');

const uglifyJS = require('uglify-js');

// JS PLUGINS
const compilePlugins = function (env) {

	const inputDir = './devAssets/plugins/';
	const outFile = process.env.publicDir + 'js/vendor.js';

	console.log('COMPILE PLUGINS');


	return new Promise(function (resolve, reject) {

		// Loop through all plugin files
		fs.readdir(inputDir, function (err, files) {
			if (err) {
				console.error("Could not list the directory.", err);
				process.exit(1);
				reject();
			}

			const allPlugins = createUglifyInput(inputDir, files);

			// RUN UGLIFY
			const js = runUglify(allPlugins, env);
			// console.log('COMPILED JS');
			// console.log(js.code);
			var fileSaved = utils.saveFile(outFile, js.code);
			fileSaved.then(function () {
				resolve();
			});
		});
	});
};

// BROWSERIFY
const runBrowserify = function (env) {
	const browserify = require('browserify');
	const b = browserify();
	const includesFile = './devAssets/browserify/mainBrowserify.js';
	const outFile = process.env.publicDir + 'js/browserify.js';

	return new Promise(function (resolve, reject) {

		b.add(includesFile);
		b.bundle(function (err, buffer) {
			if (err) {
				console.log('Error with browserify');
				console.log(err);
				reject();
				return;
			}
			// console.log(buffer);
			// console.log(buffer.toString());
			const js = buffer.toString();
			var fileSaved = utils.saveFile(outFile, js);
			fileSaved.then(function () {
				console.log('BROWSERIFY DONE');
				resolve();
			});
		});
	});
};

const compileAllJS = function () {

	const files = [
		process.env.publicDir + 'js/vendor.js',
		process.env.publicDir + 'js/browserify.js',
		process.env.publicDir + 'js/mouse/scripts.js'
	];


	const outFile = process.env.publicDir + 'js/min/scripts.js';

	const filesObject = createUglifyInput(null, files);

	return new Promise(function (resolve, reject) {

		// RUN UGLIFY
		const js = runUglify(filesObject);

		// console.log('COMPILED JS');
		// console.log(js.code);
		var fileSaved = utils.saveFile(outFile, js.code);
		fileSaved.then(function () {
			console.log('Compiled all JS');
			resolve();
		});

	});
};

function createUglifyInput(inputDir, files) {
	const filesObject = {};
	files.forEach(function (file, index) {
		let fileName = file;
		if (!inputDir) {
			fileName = path.basename(file);
		}
		if (path.extname(file) === '.js') {
			const filePath = !inputDir ? file : path.join(inputDir, fileName);
			const fileContents = fs.readFileSync(filePath, 'utf8');
			filesObject[fileName] = fileContents;
		}
	});

	return filesObject;
}

function runUglify(files, env) {
	const js = uglifyJS.minify(files, {
		compress: {
			dead_code: true,
			drop_console: env === 'dev' ? false : true,
			global_defs: {
				DEBUG: true
			}
		}
	});

	return js;
}

// Check flags and run if necessary
if (utils.checkRunFlag(process)) {
	utils.setBuildConfig()
		.then(compilePlugins)
		.then(runBrowserify)
		.then(compileAllJS);
}

module.exports = {
	compilePlugins: compilePlugins,
	runBrowserify: runBrowserify,
	compileAllJS: compileAllJS
};