const path = require('path');
const fs = require('fs');
const marked = require('marked');
const _ = require('lodash');

const utils = require('./_utils.js');

const convertMarkdownItem = function ( markdownContent ) {
	// console.log(markdownContent);
	
	return new Promise(function ( resolve, reject ) {
		// If this is a markdown file, first read the content
		if ( testIfMdown( markdownContent ) && utils.checkIfFileExists( markdownContent ) ) {
			var mdownRead = utils.readFile( markdownContent );
			mdownRead
				.then( parseMarkdown )
				.then(function ( md ) {
					// console.log('MARKDOWN PARSED');
					// console.log(md);
					resolve( md );
				});
		}
		// Else just parse the md
		else {
			
			var mdownParse = parseMarkdown( markdownContent );
			mdownParse.then(function ( md ) {
				// console.log('MARKDOWN PARSED');
				// console.log(md);
				resolve( md );
			});
		}
	});
};

function parseMarkdown ( md ) {	
	return new Promise(function ( resolve, reject ) {		
		resolve( marked( md ) );
	});
}

function testIfMdown ( value ) {
	const extName = path.extname( value );
	if ( extName === '.mdown' || extName === '.md' ) {
		return true;
	} else {
		return false;
	}
}

module.exports = {
	convertMarkdownItem: convertMarkdownItem 
};