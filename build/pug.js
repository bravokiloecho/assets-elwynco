const path = require('path');
const fs = require('fs');
const yaml = require('node-yaml');
const pug = require('pug');
const _ = require('lodash');

const mdown = require('./mdown.js');
const utils = require('./_utils.js');

const viewsDirectory = path.resolve(__dirname,'../', 'views/');
const contentDirectory = path.resolve(__dirname,'../', 'devAssets/contentData/');
const tempDataDirectory = path.resolve(__dirname,'../', 'temp/');

let rebuildData = false;

// HTML
// ----
const buildHtml = function ( env, rebuildSiteData ) {
	console.log('RUN buildHtml');
	
	const siteInfoFile = path.join( contentDirectory, 'siteInfo.yaml');
	const siteContentFile = path.join( contentDirectory, 'siteContent.yaml');
	const siteDataFile = path.join( tempDataDirectory, 'siteData.yaml');

	// DO WE NEED TO REBUILD THE SITE DATA (ie, parse mdown)?
	rebuildData = rebuildSiteData;
	if ( !rebuildData && !utils.checkIfFileExists( siteDataFile ) ) {
		rebuildData = true;
	}

	console.log('Do we need to rebuild the data? ' + rebuildData );
	

	const inputDir = viewsDirectory;
	const outputDir = process.env.publicDir;

	return new Promise(function ( resolve, reject ) {
		
		var readSiteData;
		if ( rebuildData ) {
			// Just the site info
			readSiteData = readData( siteInfoFile );
		}  else {
			// Site info & content with parsed mdown
			readSiteData = readData( siteDataFile );
		}

		readSiteData
			.then( createTextData.bind( null, siteContentFile ) )
			.then(function ( siteData ) {

				// SAVE SITE DATA IN TEMP
				if ( rebuildData ) {
					cacheSiteData( siteDataFile, siteData );
				}

				// SET BUILD ENV
				siteData = defineBuildEnv( siteData, env );

				// console.log('Compile all pug files');
				
				var compiledAllFiles = loopThroughPugFiles( inputDir, outputDir, siteData );
				compiledAllFiles.then( resolve );
			});
	});	
};

function compilePugFile ( pugFile, inputDir, outputDir, siteData ) {

	const filePath = path.join( inputDir, pugFile );
	const fileName = pugFile.split('.')[0];
	const fileType = path.extname( filePath );
	const parentDir = path.dirname( filePath );

	// console.log('PUG');
	// console.log(filePath);
	// console.log(fileName);
	// console.log(fileType);
	
	return new Promise(function (resolve, reject) {

		// Ignore Pug partials
		if ( pugFile[0] === '_' ) {
			// console.log('Partials file, ignore');
			// console.log(file);
			resolve();
			return;
		}

		// Explore directories
		if ( fs.lstatSync( filePath ).isDirectory() ) {
			// console.log('Ignore directory');
			// console.log('Pug directory');
			var dirName = path.basename(filePath);
			inputDir = inputDir + '/' + dirName + '/';
			outputDir = outputDir + dirName + '/';

			// console.log('inputDir: ' + inputDir);
			// console.log('outputDir: ' + outputDir);
			var compiledAllFiles = loopThroughPugFiles( inputDir, outputDir, siteData );
			compiledAllFiles.then( resolve );
			return;
		}

		// Ignore non-pug files
		if ( fileType !== '.pug' ) {
			resolve();
			return;
		}

		// console.log('outputDir: '+outputDir);
		// console.log('filePath: '+filePath);
		// console.log('relative path: ' + path.relative( parentDir, viewsDirectory));
		
		// SET ASSET PATH
		var relativePath = path.relative( parentDir, viewsDirectory);
		siteData.assets = relativePath;
		if ( !relativePath ) {
			siteData.assets = '.';
		}

		// BUILD HTML from Jade template and Yaml site data
		const options = {
			pretty: siteData.production ? false : true
		};

		const fn = pug.compileFile( filePath, options );
		const html = fn( siteData );
		
		const htmlFile = path.join( outputDir, `${fileName}.html` );		
		var fileSaved = utils.saveFile( htmlFile, html );
		fileSaved.then( resolve );
	});
}

function loopThroughPugFiles ( inputDir, outputDir, siteData ) {

	return new Promise(function (resolve, reject) {
		fs.readdir( inputDir, function( err, files ) {
			if ( err ) {
				console.error( "Could not list the directory.", err );
				reject();
				process.exit( 1 );
			}

			var allFilePromises = [];			

			files.forEach( function( file, index ) {
				var filePromise = compilePugFile( file, inputDir, outputDir, siteData );
				allFilePromises.push( filePromise );
			});

			Promise.all( allFilePromises ).then(function () {
				resolve();
			});
		});
	});
}

function readData ( dataFile ) {
	// READ DATA
	return new Promise(function ( resolve, reject ) {
		yaml.read( dataFile, {
			encoding: 'utf8',
			schema: yaml.schema.defaultSafe
		}, function ( err, data ) {
			if ( err ) {
				console.log('READING DATA ERROR!!!');
				console.log(err);
				reject();
				return;
			}
			resolve( data );
		});
	});
}


// MARKDOWN INTERPRETATION
function createTextData ( siteContentFile, siteData ) {
	// console.log('createTextData');
	return new Promise(function ( resolve, reject ) {

		if ( !rebuildData ) {
			resolve( siteData );
			return;
		}

		// console.log('siteData before');
		// console.log( siteData );
		
		var getSiteData = readData( siteContentFile );
		
		getSiteData.then(function ( contentData ) {

			// console.log('contentData');
			// console.log(contentData);

			// Loop through all of site content data and convert markdown
			const markdownConverted = convertAllMarkdown( contentData );
			// console.log(markdownConverted);
			markdownConverted.then(function	() {
				siteData.content = contentData;
				resolve( siteData );
			});
			
		});
	});
}



function convertAllMarkdown ( contentData ) {
	return new Promise(function (resolve, reject) {
		var allPromises = [];

		Object.keys(contentData).forEach(function (key) {
			
			var convertMarkdown = convertMarkdownItem( contentData, key );
			allPromises.push( convertMarkdown );

		});

		if ( !allPromises.length ) {
			resolve();
			return;
		}

		Promise.all( allPromises ).then(function () {
			resolve();
		});

	});
}

function convertMarkdownItem ( contentData, key) {

	const mdKey = 'md';
	
	return new Promise(function ( resolve, reject ) {
		
		// If this is an object, restart the search
		if ( contentData[key] && isObject( contentData[key] ) ) {
			const convertedMarkdown = convertAllMarkdown( contentData[key] );
			convertedMarkdown.then( resolve );
			return;
		}

		// Ignore non mdown keys
		if (key !== mdKey) {
			resolve( false );
			return;
		} 
		
		const convertMarkdown = mdown.convertMarkdownItem( contentData[key] );
		convertMarkdown.then(function ( md ) {
			contentData[key] = md;
			resolve();
		});

	});
}

function cacheSiteData( siteDataFile, siteData ) {
	utils.saveYaml( siteDataFile, siteData );
}


function isObject ( obj ) {
    return obj instanceof Object;
}


function defineBuildEnv ( data, env ) {
	if ( env === 'dev' ) {
		data.dev = true;
	} else if ( env === 'staging' ) {
		data.staging = true;
	} else {
		data.production = true;
	}
	
	return data;
}


// Check flags and run if necessary
if ( utils.checkRunFlag( process ) ) {
	utils.setBuildConfig().then( buildHtml );
}


module.exports = {
	buildHtml: buildHtml 
};