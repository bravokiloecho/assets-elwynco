const path = require('path');
const fs = require('fs');
const marked = require('marked');
const _ = require('lodash');

const utils = require('./_utils.js');

const sharp = require('sharp');


function resizeAllImages () {
	
	const inputDir = './devAssets/images/';
	const outputDir = `${process.env.publicDir}images/`;

	return new Promise(function ( resolve, reject ) {
		// Loop through all sass files
		fs.readdir( inputDir, function( err, files ) {
			if ( err ) {
				console.error( "Could not list the directory.", err );
				process.exit( 1 );
				reject('Could not list the directory.');
			}

			let allFilePromises = [];

			files.forEach( function( file, index ) {
				let filePromise = makeAllSizes( inputDir, outputDir, file );
				allFilePromises.push( filePromise );
			});

			Promise.all( allFilePromises ).then(function () {
				console.log('RESIZED ALL IMAGES');
				resolve();
			});
		});
	});
}

function makeAllSizes ( inputDir, outputDir, file ) {

	const sizes = [200,760,1000,1400];

	return new Promise(function ( resolve, reject ) {

		// Stop here if not image file
		const isImage = testIfImg( file );
		if ( !isImage ) {
			resolve( false );
			return;
		}

		let allSizesPromises = [];

		sizes.forEach( function( size ) {
			// Resize image
			let filePromise = resizeImage( inputDir, outputDir, file, size );
			allSizesPromises.push( filePromise );
		});

		Promise.all( allSizesPromises ).then(function () {
			resolve();
		});
		


	});
}

function resizeImage ( inputDir, outputDir, file, size ) {

	return new Promise(function ( resolve, reject ) {

		const filePath = path.join( inputDir, file );
		const fileName = file;

		outputDir = outputDir + size + '/';

		// Ensure directory exists
		// console.log( outputDir + fileName );
		utils.ensureDirectoryExistence( outputDir + fileName );

		// Resize image
		sharp( filePath )
			.resize( size, size, {
				fit: 'inside'
			})
			.toFile( outputDir + fileName )
			.then(info => {
				console.log('Saved image ' + outputDir + fileName);
				resolve( info );
			});

	});
}


function testIfImg ( value ) {

	const extName = path.extname( value );
	if ( extName === '.jpg' || extName === '.jpeg' || extName === '.png'  ) {
		return true;
	} else {
		return false;
	}
}

// Check flags and run if necessary
if ( utils.checkRunFlag( process ) ) {
	utils.setBuildConfig().then( resizeAllImages );
}

module.exports = {
	resizeAllImages: resizeAllImages 
};