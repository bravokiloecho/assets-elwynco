const path = require('path');
const fs = require('fs');
const utils = require('./_utils.js');

const compileAllSass = function ( env ) {
	const sass = require('node-sass');
	const inputDir = './devAssets/sass/';
	const outputDir = env === 'dev' ? 'mouse' : 'min';
	console.log('COMPILE ALL SASS');
	

	return new Promise(function ( resolve, reject ) {
		// Loop through all sass files
		fs.readdir( inputDir, function( err, files ) {
			if ( err ) {
				console.error( "Could not list the directory.", err );
				process.exit( 1 );
			}

			var allFilePromises = [];

			files.forEach( function( file, index ) {
				var filePromise = compileSassFile ( sass, inputDir, outputDir, file, env );
				allFilePromises.push( filePromise );
			});

			Promise.all( allFilePromises ).then(function () {
				resolve();
			});
		});
	});

};

function compileSassFile ( sass, inputDir, outputDir, file, env ) {

	const filePath = path.join( inputDir, file );
	const fileName = file.split('.')[0];
	return new Promise(function ( resolve, reject ) {
		
		// Ignore directories
		if ( fs.lstatSync( filePath ).isDirectory() ) {
			// console.log('Ignore directory');
			// console.log(file);
			resolve();
			return;
		}

		// Ignore Sass partials and system files
		if ( file[0] === '_' || file[0] === '.' ) {
			// console.log('Partials file, ignore');
			// console.log(file);
			resolve();
			return;
		}

		// console.log(file);

		// Compile			
		var options = {
			file: filePath,
			outFile: `${process.env.publicDir}css/${outputDir}/${fileName}.css`,
			outputStyle: env === 'dev' ? 'nested' : 'compressed',
			sourceMap: env === 'dev' ? true : false,
			sourceComments: env === 'dev' ? true : false,
		};
		
		sass.render( options, function(error, result) {
			if (error) {
				console.log(error.status); // used to be "code" in v2x and below
				console.log(error.column);
				console.log(error.message);
				console.log(error.line);
				reject();
			} else {
				console.log('DONE SASS');
				const css = result.css.toString();
				// const map = JSON.stringify(result.map);
				const map = result.map ? result.map.toString() : false;
				var fileSaved;
				if ( env === 'dev' ) {
					fileSaved = saveCss( options.outFile, css, map );
					fileSaved.then(function () {
						resolve();
					});
				} else {
					autoprefixCss( css, function ( css ) {
						fileSaved = saveCss( options.outFile, css, map );
						fileSaved.then( resolve );
					});
				}
			}
		});
	});


}

function saveCss ( filePath, css, map ) {

	return new Promise(function ( resolve, reject ) {
		var allPromises = [];
		var cssSaved = utils.saveFile( filePath, css );
		allPromises.push( cssSaved );

		if ( map ) {
			var mapSaved = utils.saveFile( filePath + '.map', map );
			allPromises.push( mapSaved );
		}

		Promise.all( allPromises ).then(function(){
			resolve();
		});
	});
}

function autoprefixCss ( css, next ) {
	const autoprefixer = require('autoprefixer');
	const postcss = require('postcss');
	const options = {
		browsers: [
			'last 4 versions',
			'last 10 Chrome versions',
			'Firefox > 15',
			'ie 9 - 11',
			'iOS >= 7'
		]
	};

	postcss([ autoprefixer( options ) ]).process(css).then(function (result) {
		result.warnings().forEach(function (warn) {
			console.warn(warn.toString());
		});
		next( css );
	});
}

// Check flags and run if necessary
if ( utils.checkRunFlag( process ) ) {
	console.log('RUN SASS BULD');
	utils.setBuildConfig().then( compileAllSass );
}

module.exports = {
	compileAllSass: compileAllSass
};