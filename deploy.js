const utils = require('./build/_utils');
const build = require('./build/index');


var config = {
	region: 'eu-west-2',
	domain: 'test.dev.elwyn.co',
	deploy: true,
	index: 'index.html',
	signatureVersion: 'v4',
	uploadDir: undefined // defined in deploySite
};


// DEPLOY
// --------------------
function deploySite () {

	console.log('*****************');
	console.log('DEPLOY');
	console.log('*****************');
	
	config.uploadDir = process.env.publicDir;
	const deploy = require('s3-website').deploy;
	const AWS = require('aws-sdk');
	const s3 = new AWS.S3({
		region: config.region,
	});

	return new Promise(function ( resolve, reject ) {
		deploy(s3, config, (err, website) => {
			if( err ) {
				reject( err );
				return;
			}
			resolve();
		});
	});
}

// CREATE
// -------------------
function createSite () {

	console.log('*****************');
	console.log('CREATE SITE');
	console.log('*****************');

	return new Promise(function ( resolve, reject ) {
		const create = require('s3-website').s3site;

		create( config, (err, website) => {
		  if( err ) {
				reject( err );
				return;
  			}
		  console.log(website);
		  resolve( website );
		});
	});

}

function getAwsCredentials () {
	const path = require('path');
	const credentials = path.resolve( '../aws/credentials_2.yml' );
	
	return new Promise(function ( resolve, reject ) {
		utils.readYaml( credentials ).then(function( credentials ) {
			process.env.AWS_ACCESS_KEY_ID = credentials.accessKeyId;
			process.env.AWS_SECRET_ACCESS_KEY = credentials.secretAccessKey;
			resolve( credentials );			
		});
	});
}

function openInBrowser( url ) {
	const opn = require('opn');
	url = url || 'http://' + config.domain;
	opn( url, {
		wait: false
	});
}

const run = function () {

	const processArgs = process.argv;
	const flag = processArgs[processArgs.length - 1];

	// Set env
	var env = 'production';
	if ( flag === '-s' || flag === '-n' ) {
		env = 'staging';
	}

	// Check which process to run
	var deployFunction;
	
	if ( flag === '-n') {
		deployFunction = createSite;
	} else {
		deployFunction = deploySite;
	}

	// GO!
	utils.setBuildConfig().then(function () {
		// Set upload directory
		config.uploadDir = process.env.publicDir;
		build.buildAll( env )
			.then( getAwsCredentials )
			.then( deployFunction )
			.catch(function ( err ) {
				console.log('ERROR');
				console.log(err);
			})
			.then( openInBrowser );
	});
};

run();