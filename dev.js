const utils = require('./build/_utils');
const build = require('./build/index');
const port = 3002;
const devUrl = 'http://localhost:' + port;

function startServer () {

	var url;

	if ( process.env.buildType === 'static') {
		var finalhandler = require('finalhandler');
		var http = require('http');
		var serveStatic = require('serve-static');

		// Serve up public/ftp folder
		var serve = serveStatic(process.env.publicDir, {'index': ['index.html']} );

		// Create server
		var server = http.createServer(function onRequest (req, res) {
		  serve(req, res, finalhandler(req, res));
		});

		// Listen
		console.log( 'Listening at: ' + devUrl );
		server.listen(port);
	} else {
		url = process.env.localUrl;
	}

	openInBrowser( url );
}

function startBrowserSync ( next ) {
	// require the module as normal
	const bs = require("browser-sync").create();
	const fileExtensions = 'html,js,css,png,gif,jpg,svg,json,txt,php,inc,twig';


	const watchFiles = [ process.env.publicDir + '**/*' + `.{${ fileExtensions }}` ];
	
	var bsPort = port;
	if ( process.env.buildType === 'cms' ) {
		bsPort = 3002;
		watchFiles.push( process.env.templatesDir + '**/*' + `.{${ fileExtensions }}` );
	}

	// Setup https
	var https = false;
	// if ( process.env.buildType === 'cms' ) {
	// 	https = {
	// 		key: process.env.key,
	// 		cert: process.env.crt,
	// 	};
	// }

	// .init starts the server
	bs.init({
		// Watch changes in the public directory
		files: watchFiles,
		port: bsPort,
		https: https
	});

	// WATCH FILES

	// Static build specific
	if ( process.env.buildType === 'static') {
		watchData( bs );
		watchMarkdown( bs );
		watchPug( bs );
	}
	
	watchSass( bs );
	watchCoffee( bs );
	watchBrowserify( bs );
	watchPlugins( bs );
	watchSVGs( bs );
	watchPublicAssets( bs );
}

function openInBrowser( url ) {
	const opn = require('opn');
	url = url || devUrl;
	opn( url );
}

function watchData ( bs ) {
	const files = [
		'./devAssets/contentData/*.yaml',
	];

	bs.watch( files ).on("change", function () {
		build.buildHtml( 'dev', true );
	});
}

function watchMarkdown ( bs ) {
	const files = [
		'./devAssets/text/**'
	];

	bs.watch( files ).on("change", function () {
		build.buildHtml( 'dev', true );
	});
}

function watchPug ( bs ) {
	const files = [
		'./views/**',
	];

	bs.watch( files ).on("change", function () {
		build.buildHtml( 'dev', false );
	});
}

function watchCoffee ( bs ) {
	const files = [
		'./devAssets/coffee/**',
	];

	bs.watch( files ).on("change", function () {
		build.compileCoffee( 'dev' );
	});
}

function watchPlugins ( bs ) {
	const files = [
		'./devAssets/plugins/**',
	];

	bs.watch( files ).on("change", function () {
		build.compilePlugins( 'dev' );
	});
}

function watchBrowserify ( bs ) {
	const files = [
		'./devAssets/browserify/**',
	];

	bs.watch( files ).on("change", function () {
		build.runBrowserify( 'dev' );
	});
}

function watchSass ( bs ) {
	const files = [
		'./devAssets/sass/**',
	];

	bs.watch( files ).on("change", function () {
		build.compileAllSass( 'dev' );
	});
}

function watchSVGs ( bs ) {
	const files = [
		'./devAssets/svg/**',
	];

	bs.watch( files ).on("change", function () {
		build.minifySVGs( 'dev' );
	});
}

function watchPublicAssets ( bs ) {
	const files = [
		process.env.publicAssetsDir + '**',
	];

	bs.watch( files ).on("change", function () {
		build.copyAssets( 'dev' );
	});
}

function start () {
	utils.setBuildConfig().then(function () {
		// console.log('PROCESS ENV');
		// console.log(process.env);
		build.buildAll( 'dev', function (){
			startServer();
			startBrowserSync();
		});
	});
}



start();