Global = do ->

	isMobile = null
	isTablet = null
	isDevice = null
	isTouch = null

	$html = null
	$body = null
	$wrapper = null

	init = ->
		# DEFINE GLOBAL VARIABLES HERE
		gv = window.gv =
			$window: $(window)
			$document: $(document)
			$html: $('html').first()
			$body: $('body').first()
			WIDTH: $(window).width()
			HEIGHT: $(window).height()
			# deviceVariables: Elwyn.defineDeviceVariables()
			isDev: window.isDev
			$wrapper: $ '#wrapper'
			# browser: Elwyn.browserDetect()

		$html = gv.$html
		$body = gv.$body
		$wrapper = gv.$wrapper

		# Sitewide vars
		gv.siteURL = Elwyn.stripTrailingSlash window.siteURL

		# isMobile = gv.deviceVariables.isMobile
		# isTablet = gv.deviceVariables.isTablet
		# isDevice = gv.deviceVariables.isDevice
		# isTouch = gv.deviceVariables.isTouch

		# # IS DEKSTOP?
		# if !isMobile && !isTablet
		# 	gv.isDesktop = true
		# else
		# 	gv.isDesktop = false
	
		# Get browser version
		browser = Elwyn.browserDetect()
		# console.log 'Browser version'
		# console.log browser

		do backToTopOnClick
		do logLinkClicks
		do testLegacyWarning
		# do setDeviceBodyClasses

		if window.isDev
			do logDeviceData

		# FASTCLICK
		attachFastClick document.body


	logLinkClicks = ->
		$body.on 'click', 'a', ->
			href = $(this).attr 'href'
			Class = $(this).attr 'class'
			label = if href then href else Class
			# category, action, label
			Elwyn.trackEvent 'linkClick', 'click', label


	backToTopOnClick = ->
		$wrapper.on 'click', '.back-to-top', ->
			scrollTo 0

	# toggleFullscreen = ->

	# 	# Uses fullscreen plugin
	# 	# https://github.com/private-face/jquery.fullscreen

	# 	isFullscreen = $.fullscreen.isFullScreen()

	# 	if !isFullscreen
	# 		gv.$body.fullscreen()
	# 	else
	# 		$.fullscreen.exit()

	scrollTo = ( offset, speed, $target, next ) ->
		
		# Uses velocity.js

		offset = offset || 0
		speed = speed || 200
		$target = $target || $ 'html'

		$target.velocity 'scroll',
			duration: speed
			offset: offset
			easing: 'ease-out'


	setDeviceBodyClasses = ->
		
		if isMobile
			$body
			.addClass 'is-mobile'
			.addClass 'is-device'
			return

		if isTablet
			$body
			.addClass 'is-tablet'
			.addClass 'is-device'
			return


	testLegacyWarning = ->
		$html = $('html').first()
		ieTest = $html.attr('data-ie')
		
		if ieTest is 'ie7'
			$html.addClass 'ie7'
		else if ieTest is 'ie8'
			$html.addClass 'ie8'
		else if ieTest is 'ie9'
			$html.addClass 'ie9'

	logDeviceData = ->
		if device
			console.log 'is mobile: '+device.mobile()
			console.log 'is tablet: '+device.tablet()
			console.log 'is desktop: '+device.desktop()

	{
		init: init
		# toggleFullscreen: toggleFullscreen
		scrollTo: scrollTo
	}