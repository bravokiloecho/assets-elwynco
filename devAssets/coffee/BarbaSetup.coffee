BarbaSetup = do ->

	mousePos = false

	start = ->

		console.log 'BARBA SETUP'

		do setConfigOptions

		do setupCustomTransition

		do listenToInteractionEvents

		do listenToPageEvents

		# Start it
		Barba.Pjax.start()
		
		do setupViews


	setConfigOptions = ->
		Barba.Pjax.Dom.wrapperId = 'pjax-wrapper'
		Barba.Pjax.Dom.containerClass = 'pjax-container'
		Barba.Pjax.cacheEnabled = false

	listenToInteractionEvents = ->
		Barba.Dispatcher.on 'linkClicked', ( el, mouse ) ->
			mousePos =
				x: mouse.clientX
				y: mouse.clientY


	# Set a custom transition so content fades in and out
	# See here: http://barbajs.org/transition.html
	setupCustomTransition = ->

		# DEFINE TRANSITION
		customTransition = Barba.BaseTransition.extend
			start: ->
				Promise
					.all [ this.newContainerLoading, this.pageOut() ]
					.then this.pageIn.bind( this )

			pageOut: ->
				deferred = Barba.Utils.deferred()
				PageTransition.moveOut $(this.oldContainer), mousePos, deferred
				return deferred.promise

			pageIn: ->
				console.log 'promise'
				console.log this
				PageTransition.moveIn $(this.newContainer), $(this.oldContainer), this
				mousePos = false
				

		# then use new transition
		Barba.Pjax.getTransition = ->
			return customTransition


	# SETUP VIEWS
	# http://barbajs.org/views.html
	setupViews = ->

		# HOME PAGE VIEW
		HomeView = Barba.BaseView.extend
			namespace: 'homepage'
			
			# The new Container is ready and attached to the DOM
			onEnter: ->
				console.log 'Barba: enter homepage'
				return
			
			# The Transition has just finished.
			onEnterCompleted: ->
				return
			
			# A new Transition toward a new page has just started
			onLeave: ->
				console.log 'Barba: leave homepage'
				return
			
			# The Container has just been removed from the DOM.
			onLeaveCompleted: ->
				return


		# Project PAGE VIEW
		ProjectView = Barba.BaseView.extend
			namespace: 'project'

			# The new Container is ready and attached to the DOM
			onEnter: ->
				console.log 'Barba: enter work item'
				# PageSetup.run 'project'

				return
			
			# The Transition has just finished.
			onEnterCompleted: ->
				return
			
			# A new Transition toward a new page has just started
			onLeave: ->
				console.log 'Barba: leave work item'
				return
			
			# The Container has just been removed from the DOM.
			onLeaveCompleted: ->
				return
		
		# Init the views
		HomeView.init()
		ProjectView.init()


	# LISTEN TO PAGE READY
	# "The new container has been loaded and injected in the wrapper."
	listenToPageEvents = ->
		Barba.Dispatcher.on 'newPageReady', ( currentStatus, oldStatus, container ) ->
			onPageReady currentStatus, oldStatus, container

		Barba.Dispatcher.on 'transitionCompleted', ( currentStatus, oldStatus ) ->
			onTransitionCompleted currentStatus, oldStatus


	onPageReady = ( currentStatus, oldStatus, container ) ->

		console.log 'ON PAGE READY'

		console.log 'NEW STATUS'
		console.log currentStatus

		# Stop here if this is the first time
		if not oldStatus.namespace
			console.log 'FIRST TIME LOAD'
			return

		console.log 'oldStatus'
		console.log oldStatus

		newPageData = $(container).data()
		console.log 'NEW PAGE DATA'
		console.log newPageData

		# Set new title
		# Set the page title
		Dom.onPageChange newPageData

	onTransitionCompleted = ( currentStatus, oldStatus ) ->
		console.log 'ON TRANSITION COMPLETED'
		console.log 'currentStatus: '+currentStatus.namespace
		if oldStatus
			console.log 'oldStatus: '+oldStatus.namespace
		


	# EXPORT
	{
		start: start
	}