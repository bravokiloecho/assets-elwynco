Controller = do ->

	init = ( next = $.noop ) ->

		console.log 'Current page is: '+currentPage

		initialPage = window.currentPage

		pageReady = $.Deferred()
		$pageIn = $( '.pjax-container', gv.$wrapper ).first()

		do keyboardShortcuts

		if Modernizr.history and not device.mobile()
			do BarbaSetup.start

		PageSetup.run $pageIn, initialPage, pageReady, 'initial'

		$.when( pageReady ).then ->
			# LoadingOverlay.hide $pageIn
			console.log 'Page Setup Ready'
		


				
	# MISC FUNCTIONS
	# ##############

	keyboardShortcuts = ->
		if not window.isDev
			return
		# Dev only shortuts
		# Mousetrap.bind 'l', ->
		# 	do LoadingOverlay.show

	{
		init: init
	}