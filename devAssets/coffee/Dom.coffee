Dom = do ->

	$title = null
	$links = null
	$description = null

	setup = ->
		$title = $('#meta-title')
		$description = $('#meta-description')
		# $links = $('#hamburger-menu',gv.$wrapper).find('.hamburger-link')

	onPageChange = ( newPageData ) ->
		title = newPageData.title
		description = newPageData.description
		namespace = newPageData.namespace
		# Update title and description
		setMetaInfo title, description


	setMetaInfo = ( title, description ) ->
		if title
			$title.text title
		if description
			$description.text description

	setLinks = ( slug ) ->
		activeClass = 'active'
		$activeLink = $links.filter '.' + slug

		# console.log "Switch links: #{ slug }."
		# console.log $links.filter( '.' + slug).length

		# $links
		# 	.removeClass activeClass
		# 	.filter '.' + slug
		# 	.addClass activeClass


	{
		setup: setup
		onPageChange: onPageChange
	}