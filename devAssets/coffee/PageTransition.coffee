PageTransition = do ->

	$ = window.jQuery

	fadeDuration =
		out: 250
		in: 350

	moveOut = ( $pageOut, mousePos, promise ) ->
		pageOut = $pageOut.data 'namespace'
		console.log 'MOVE PAGE OUT: ' + pageOut
		$pageOut.hide()
		promise.resolve()


	moveIn = ( $pageIn, $pageOut, promise ) ->
		pageIn = $pageIn.data 'namespace'
		pageOut = $pageOut.data 'namespace'
		console.log 'MOVE PAGE IN: ' + pageIn

		window.currentPage = pageIn		

		$pageIn.css 
			visibility: 'visible'
			opacity: 0

		# console.log 'MOVE PAGE IN: '+pageIn
		console.log $pageIn[0]
		pageReady = $.Deferred()
		PageSetup.run $pageIn, pageIn, pageReady

		$.when( pageReady ).then ->
			console.log 'Page Setup Ready'
			gv.$window.scrollTop 0
			fadePageIn $pageIn, ->
				promise.deferred.resolve()
				promise.done()


	# fadePageOut = ( $page, promise ) ->
	# 	$page.velocity {
	# 		opacity: 0
	# 	},{
	# 		queue: false
	# 		duration: fadeDuration.out
	# 		complete: ->
	# 			promise.resolve()
	# 	}


		
	fadePageIn = ( $page, next ) ->
		$page.opacity 1
		do next


	{
		moveOut: moveOut
		moveIn: moveIn
		fadePageIn: fadePageIn
	}